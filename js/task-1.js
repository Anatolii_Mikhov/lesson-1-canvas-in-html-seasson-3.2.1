var canvas = document.getElementById('primitive');
var context = canvas.getContext('2d');

// Drawing Rectangle
context.fillStyle = '#2f1fea';
context.fillRect(50, 50, 250, 90);


// Drawing circle
context.lineWidth = 10;
context.strokeStyle = '#791c00';
context.fillStyle = '#352d90';
context.beginPath();

context.arc(450,200,150,0,2*Math.PI);

context.closePath();
context.stroke();
context.fill();

// Drawing Text
context.font = "32px Arial";
context.lineWidth = 1;
context.fillStyle = '#a59f00';
context.strokeStyle = '#6a1901';
// Text Rectangle
context.fillText('The Rectangle', 50, 180);
context.strokeText('The Rectangle', 50, 180);
// Text Circle
context.font = "36px Arial";
context.fillText('The Rectangle', 330, 210);
context.strokeText('The Rectangle', 330, 210);

